/*
 * Copyright (C) 2013 Wolfram Rittmeyer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.grokkingandroid.sampleapp.samples.actionbar.actionviews;

import android.os.AsyncTask;
import android.os.SystemClock;
import android.support.v4.view.MenuItemCompat;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;


public class PlaceholderActionViewFragment extends DemoBaseFragment {

	private MenuItem mProgress;

   public static PlaceholderActionViewFragment newInstance() {
      return new PlaceholderActionViewFragment();
   }

	@Override
   public String getDescriptionText() {
      return getResources().getString(R.string.placeholder_desc);
   }
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.menu_fragment_placeholder, menu);
		this.mProgress = menu.findItem(R.id.progress);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.progress) {
			new DummyAsyncTask().execute((Void[]) null);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private class DummyAsyncTask extends AsyncTask<Void, Void, Void> {
		@Override
		protected Void doInBackground(Void... params) {
			// simulate doing some time-consuming stuff:
			SystemClock.sleep(2000);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			MenuItemCompat.setActionView(
			      PlaceholderActionViewFragment.this.mProgress, 
			      null);
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			MenuItemCompat.setActionView(
			      PlaceholderActionViewFragment.this.mProgress, 
			      R.layout.actionview_progress);
		}

	}
}
